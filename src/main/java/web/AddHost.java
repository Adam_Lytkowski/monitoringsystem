package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Host;
import services.HostResource;

@WebServlet("/add")
public class AddHost extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException {
		
		HostResource hr = new HostResource();
		Host newhost = retrieveHostFromRequest(request,response);
		
		hr.add(newhost);
		response.sendRedirect("allhosts.jsp"); 
		}	
	
	private Host retrieveHostFromRequest (HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Host result = new Host();
		result.setSocket(request.getParameter("socket"));
		result.setHostname(request.getParameter("hostname"));
		result.setUser(request.getParameter("user"));
		result.setPassword(request.getParameter("password"));
		return result;
	}
	}	