package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import domain.Host;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.HostResource;

@WebServlet("/HostListServlet")
public class HostListServlet  extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public HostListServlet() {
		super();
	}
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();  
		HostResource hr = new HostResource();	
		List<Host> allhosts = hr.getAll();
		request.setAttribute("hosts",allhosts);
		
		/*if(allhosts==null)
			out.println("Nic tu nie ma");
		out.println("<h1>All hosts list</h1>");
		out.println("<table>");
		out.println("<tr><th>Id</th><th>Socket</th><th>Hostname</th><th>User</th></tr>");
		for (Host host : allhosts) {
			out.println("<tr><td>" + host.getId() + "</td>" + "<td>"
					+ host.getSocket() + "</td>" + "<td>" + host.getHostname() + "</td>" + "<td>" + host.getUser() + "</td></tr>");
		}
		out.println("</table>");
		*/
		
        RequestDispatcher view=request.getRequestDispatcher("allhosts.jsp");
        view.forward(request,response);
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
