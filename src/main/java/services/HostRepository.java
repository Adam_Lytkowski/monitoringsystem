package services;

import java.util.List;

import domain.Host;

public interface HostRepository {
public boolean add(Host host);
public List<Host> getAll();
}
