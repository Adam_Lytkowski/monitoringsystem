package services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.core.Context;


import domain.Host;


public class HostResource implements HostRepository{
	private static List<Host> db = new ArrayList<Host>();
	
	
	public boolean add(Host host) {
		boolean isHostNew = db.stream().noneMatch(item -> {
			return item.getHostname().equals(host.getHostname());
		});

		if (isHostNew) {
			db.add(host);
			return true;
		} else {
			return false;
		}
	}
	
	public List<Host> getAll() {
		return db;
	}
	
	
}
