<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="style.css" rel="stylesheet" type="text/css"/>
<title>Monitoring</title>
</head>
<body>
<div id="container">
	<div id="header">
		<h1>Monitoring System</h1>
	</div>
	<div id="content">
		<div id="nav">
		<h3>Navigation</h3>
		<ul>
			<li><a class="selected" href="link.html">Home</a></li>
			<li><a href="allhosts.jsp">View all hosts</a></li>
		</ul>
		</div>
		<div id="main">
			<form action="add" method="get">
				<label>Socket: <input type="text" name="socket" id="socket"/></label><br><br>
				<label>Hostname: <input type="text" name="hostname" id="hostname"/></label><br><br>
				<label>User: <input type="text" name="user" id="user"/></label><br><br>
				<label>Password: <input type="text" name="password" id="password"/></label><br><br>
			<input type="submit" value="Submit"/>
	</form>
		</div>
	</div>
	<div id="footer">
	Copyright &copy; 2018 Adam Łytkowski, Adam Kordecki, Mikołaj Chodkowski
	
	</div>
</div>
		    
</body>
</html>