<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="style.css" rel="stylesheet" type="text/css"/>
<title>Monitoring</title>
</head>
<body>
<div id="container">
	<div id="header">
		<h1>Monitoring System</h1>
	</div>
	<div id="content">
		<div id="nav">
		<h3>Navigation</h3>
		<ul>
			<li><a class="selected" href="link.html">Home</a></li>
			<li><a href="add.jsp">Add Host</a></li>
		</ul>
		</div>
		<div id="main">
		${requestScope.hosts}
		</div>
	</div>
	<div id="footer">
	Copyright &copy; 2018 Adam Lytkowski, Adam Kordecki, Mikolaj Chodkowski
	
	</div>
</div>
		    
</body>
</html>